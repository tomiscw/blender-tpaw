# Blender snap

A unofficial snap package of Blender.

## Current state

Working features:
 - Snap builds
 - "SoftwareGL" mode works under ``strict`` confinement
 - All precompiled binaries work in ``classic`` mode

Known issues:
  - Renders poorly in strict confinement. "SoftwareGL" mode is the only known workaround but at the cost performance.

## What is Snap?
Snappy is a transactional package manager originally designed and built by Canonical Ltd. for the Ubuntu operating system and now available across a range of Linux distributions. The tool provides atomic, or "transactional" software updates. A single snap can work across those Linux environments, making snaps a universal Linux package. - Wikipedia

# FAQ
## How do I build a snap?
It's surprisingly simple. Assuming snapcraft is installed, type in `snapcraft` from the terminal and it'll build everything that's needed.

## How do install a snap?
Unfortunately, there is no graphical installer for snaps as of this writing. You'll have to type in `snap install --force-dangerous [package].snap` from the terminal.

## Badges

![Download for Linux](https://cdn.rawgit.com/tomascw/tms-snaps/master/badges/svg/Snaps_Badge.svg) ![Download for Linux](https://cdn.rawgit.com/tomascw/tms-snaps/master/badges/svg/Snaps_Inverted_Badge.svg)
